import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Product } from './product/entities/product.entity';
import { ProductsModule } from './product/product.module';
import { UsersModule } from './users/users.module';

@Module({
  // imports: [UsersModule],
  imports: [ProductsModule],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule {}
