import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';

@Controller('products')
export class ProductController {
  constructor(private readonly ProductService: ProductService) {}

  @Post()
  create(@Body() CreateProductDto: CreateProductDto) {
    return this.ProductService.create(CreateProductDto);
  }

  @Get()
  findAll() {
    return this.ProductService.findAll();
  }

  @Get('reset/')
  reset() {
    return this.ProductService.reset();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ProductService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() UpdateProductDto: UpdateProductDto) {
    return this.ProductService.update(+id, UpdateProductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ProductService.remove(+id);
  }
}
