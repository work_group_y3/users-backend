import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'ข้าวผัดหมูกรอบ', price: 50 },
  { id: 2, name: 'ก๋วยเตี๋ยวเส้นใหญ่', price: 45 },
  { id: 3, name: 'กระเพราหมูหมึกกุ้ง', price: 70 },
];

let lastPorductId = 4;
@Injectable()
export class ProductService {
  create(CreateProductDto: CreateProductDto) {
    const newProdut: Product = {
      id: lastPorductId++,
      ...CreateProductDto,
    };
    products.push(newProdut);
    return newProdut;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((products) => {
      return products.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, UpdateProductDto: UpdateProductDto) {
    const index = products.findIndex((products) => {
      return products.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('product ' + JSON.stringify(products[index]));
    // console.log('update ' + JSON.stringify(UpdateProductDto));
    const updateProduct: Product = {
      ...products[index],
      ...UpdateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((products) => {
      return products.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProducts = products[index];
    products.splice(index, 1);
    return deletedProducts;
  }

  reset() {
    products = [
      { id: 1, name: 'ข้าวผัดหมูกรอบ', price: 50 },
      { id: 2, name: 'ก๋วยเตี๋ยวเส้นใหญ่', price: 45 },
      { id: 3, name: 'กระเพราหมูหมึกกุ้ง', price: 70 },
    ];
    lastPorductId = 4;
    return 'RESET';
  }
}
